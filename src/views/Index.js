
import React, { Component } from "react";
import Styles from '../styles/Styles'
import { 
    View,
    Text,
    ActivityIndicator,
    ScrollView,
    FlatList,
    TouchableOpacity
} from "react-native";


class CardItem extends Component
{
    render()
    {
        return(
            <View style={Styles.item}>
                <Text style={Styles.textInfo}>Nombre: {this.props.nombre+" "+this.props.apellidoPaterno+" "+this.props.apellidoMaterno}</Text>
                <Text style={Styles.textInfo}>Télefono: {this.props.telefono}</Text>
                <Text style={Styles.textInfo}>SAP: {this.props.sap}</Text>
                <Text style={Styles.textInfo}>Fecha: {this.props.fechaCreacion}</Text>
                <Text style={Styles.textInfo}>N. Fiscal: {this.props.nombre_fiscal}</Text>
                <Text style={Styles.textInfo}>RFC: {this.props.rfc}</Text>
                <Text style={Styles.textInfo}>Contacto: {this.props.contacto}</Text>
                <Text style={Styles.textInfo}>Domiclio: {this.props.municipio+" / "+this.props.estado}</Text>
                <View style={Styles.botones}>
                    <TouchableOpacity onPress={() => this.props.navegador.navigate('Cliente', {
                                                        idcliente:this.props.idcliente
                                                    })} style={Styles.colspanEdit}>
                        <Text style={Styles.textButton}>Editar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}


class Index extends Component 
{
    constructor(props)
    {
        super(props)
        this.state = {
            isLoading: true,
            clientes: {}
        }
    }

    componentDidMount()
    {   
        this.refresh();       
    }

    refresh = () =>
    {
        // console.log(this.props.navigation);
        const reflesh = this.props.navigation.addListener('focus', () => {
            this.getAllClientes();  
          });
      
        return reflesh;
    }


    getAllClientes = () =>
    {
        fetch('https://omartrejo.000webhostapp.com/Services/Server/functions/clientes.php', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        }).then((response) => response.json()).then((json) => {
            this.setState({
                    clientes: json,
                    isLoading:false
                })
          return json;
        })
        .catch((error) => {
          console.error(error);
        });
    }

    

    render()
    {
        return(
            <ScrollView style={Styles.container}>
                {this.state.isLoading && <ActivityIndicator size="large" color="#0000ff" ></ActivityIndicator>}
                <View style={Styles.body}>
                    <Text style={Styles.title}>Clientes registrados</Text>    
                    <TouchableOpacity style={Styles.botonRegistro} onPress={() => this.props.navigation.navigate('Cliente')}> 
                        <Text style={Styles.textButton}>Nuevo registro</Text>    
                    </TouchableOpacity>   
                    {this.state.clientes.length > 0 && 
                        <View style={Styles.section}>
                            { this.state.clientes.map((item) => (
                                <CardItem
                                    key={item.idcliente}
                                    nombre={item.nombre}
                                    apellidoPaterno={item.apellidoPaterno}
                                    apellidoMaterno={item.apellidoMaterno}
                                    telefono={item.telefono}
                                    sap={item.sap}
                                    nombre_fiscal={item.nombre_fiscal}
                                    rfc={item.rfc}
                                    fechaCreacion={item.fechaCreacion}
                                    estado={item.estado}
                                    municipio={item.municipio}
                                    contacto={item.contacto}
                                    idcliente={item.idcliente}
                                    navegador={this.props.navigation}
                                ></CardItem>
                            ))}
                        </View>
                    }
                    
                </View>
            </ScrollView>
        )
    }
}

export default Index;