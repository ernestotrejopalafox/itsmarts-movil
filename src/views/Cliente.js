import React, { Component } from "react";
import { View, Text, ActivityIndicator, ScrollView, TouchableOpacity } from "react-native";
import Styles from '../styles/Styles'
import { TextInput } from "react-native-gesture-handler";
import {Picker} from '@react-native-community/picker';
import InputTime from '../components/datetimepicker';

class NewCliente extends Component
{
    constructor(props)
    {
        super(props)
        this.state = {
            isEdit:false,
            isLoading: true,
            estados:{},
            municipios:{},
            nombre:"",
            apellidoPaterno:"",
            apellidoMaterno:"",
            telefono:"",
            sap:"",
            fechaCreacion:"2020-02-20 00:00:00",
            nombre_fiscal:"",
            rfc:"",
            contacto:"",
            idestado:1,
            idmunicipio:"",
            latitud:"",
            longitud:"",
            idcliente:0,
            date:new Date(1598051730000)
        }
    }

    componentDidMount()
    {
        this.obtenerEstados();
        if (this.props.route.params) 
        {
            this.setState({
                isEdit:true,
                isLoading:false
            })
            this.obtenerCliente(this.props.route.params.idcliente);
        }
        else
        {
            this.setState({
                isLoading:false
            })
        }
        
    }

    obtenerCliente = (idcliente) => 
    {
        fetch('https://omartrejo.000webhostapp.com/Services/Server/functions/cargarCliente.php', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({
                id: idcliente
            }),
            }).then((response) => response.json()).then((json) => {
                this.setState({
                        idcliente:json.idcliente,
                        nombre:json.nombre,
                        apellidoPaterno:json.apellidoPaterno,
                        apellidoMaterno:json.apellidoMaterno,
                        telefono:json.telefono,
                        sap:json.sap,
                        fechaCreacion:json.fechaCreacion,
                        nombre_fiscal:json.nombre_fiscal,
                        rfc:json.rfc,
                        contacto:json.contacto,
                        idestado:json.idestado,
                        idmunicipio:json.idmunicipio,
                        latitud:json.latitud,
                        longitud:json.longitud
                    })
                this.obtenerMunicipios(json.idestado);
              return json;
            })
            .catch((error) => {
              console.error(error);
            });
    }
    
    delCliente = () => 
    {
        
        fetch('https://omartrejo.000webhostapp.com/Services/Server/functions/delCliente.php', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({
                id: this.state.idcliente
            }),
            }).then((response) => response.json()).then((json) => {
                alert("Cliente eliminado con éxito");
                this.props.navigation.navigate("Index");
              return json;
            })
            .catch((error) => {
              console.error(error);
            });
    }

    addCliente = () =>
    {
        //CREAR EL OBJETO
        if (this.state.idmunicipio === "") 
        {
            alert("Te faltan datos por seleccionar");
        }
        else
        {
            const obj = {
                nombre:this.state.nombre,
                apellidoPaterno:this.state.apellidoPaterno,
                apellidoMaterno:this.state.apellidoMaterno,
                telefono:this.state.telefono,
                sap:this.state.sap,
                fechaCreacion:this.state.fechaCreacion,
                nombre_fiscal:this.state.nombre_fiscal,
                rfc:this.state.rfc,
                contacto:this.state.contacto,
                idestado:this.state.idestado,
                idmunicipio:this.state.idmunicipio,
                latitud:this.state.latitud,
                longitud:this.state.longitud,
            }
            
            fetch('https://omartrejo.000webhostapp.com/Services/Server/functions/addCliente.php', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify(obj),
            }).then((response) => response.json()).then((json) => {
                alert("Registrado con éxito");
                this.props.navigation.navigate("Index");
              return json;
            })
            .catch(function(error) {
                alert(error);   // Using this line
            });
        }
        

       
    }

    updateCliente = () =>
    {
        //CREAR EL OBJETO
        if (this.state.idmunicipio === "") 
        {
            alert("Te faltan datos por seleccionar");
        }
        else
        {
            const obj = {
                idcliente:this.state.idcliente,
                nombre:this.state.nombre,
                apellidoPaterno:this.state.apellidoPaterno,
                apellidoMaterno:this.state.apellidoMaterno,
                telefono:this.state.telefono,
                sap:this.state.sap,
                fechaCreacion:this.state.fechaCreacion,
                nombre_fiscal:this.state.nombre_fiscal,
                rfc:this.state.rfc,
                contacto:this.state.contacto,
                idestado:this.state.idestado,
                idmunicipio:this.state.idmunicipio,
                latitud:this.state.latitud,
                longitud:this.state.longitud,
            }

            
            fetch('https://omartrejo.000webhostapp.com/Services/Server/functions/updateCliente.php', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify(obj),
            }).then((response) => response.json()).then((json) => {
                alert("Actualizado con éxito");
                this.props.navigation.navigate("Index");
              return json;
            })
            .catch(function(error) {
                alert(error);   // Using this line
            });
        }
        

       
    }


    obtenerEstados = () => 
    {
        fetch('https://omartrejo.000webhostapp.com/Services/Server/functions/estados.php', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            }).then((response) => response.json()).then((json) => {
                this.setState({
                        estados: json
                    })
              return json;
            })
            .catch((error) => {
              console.error(error);
            });
    }

    obtenerMunicipios = (id) =>
    {
        fetch('https://omartrejo.000webhostapp.com/Services/Server/functions/municipios.php', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({
                id: id
            }),
            }).then((response) => response.json()).then((json) => {
                this.setState({
                        municipios:json
                    })
              return json;
            })
            .catch((error) => {
              console.error(error);
            });
    }

    handleChange = (itemValue)=>{
        this.setState({idestado: itemValue});
        this.obtenerMunicipios(itemValue);
    }

    handleChangeMunicipio = (itemValue)=>{
        this.setState({idmunicipio: itemValue});
    }

    render()
    {
        return(
            <ScrollView style={Styles.container}>
                {this.state.isLoading && <ActivityIndicator size="large" color="#0000ff" ></ActivityIndicator>}
                <View style={Styles.body}>
                    {this.state.isEdit ? <Text style={Styles.title}>Editar registro </Text> : <Text style={Styles.title}>Nuevo registro </Text>}
                    <View style={Styles.colspan3}>
                    </View>
                    {this.state.isEdit && <TouchableOpacity onPress={() => this.delCliente(this.props.idcliente)} style={Styles.colspanDelete}>
                        <Text style={Styles.textButton}>Eliminar</Text>
                    </TouchableOpacity>
                    }
                   
                    <View style={Styles.form}>
                        <Text style={Styles.textForm}>Nombre:</Text>
                        <TextInput style={Styles.inputForm} 
                        value={this.state.nombre}
                        onChangeText={(text) => this.setState({nombre:text})}
                        ></TextInput>
                    </View>
                    <View style={Styles.form}>
                        <Text style={Styles.textForm}>Apellido paterno:</Text>
                        <TextInput style={Styles.inputForm} value={this.state.apellidoPaterno}
                        onChangeText={(text) => this.setState({apellidoPaterno:text})}
                        ></TextInput>
                    </View>
                    <View style={Styles.form}>
                        <Text style={Styles.textForm}>Apellido materno:</Text>
                        <TextInput style={Styles.inputForm} value={this.state.apellidoMaterno}
                        onChangeText={(text) => this.setState({apellidoMaterno:text})}></TextInput>
                    </View>
                    <View style={Styles.form}>
                        <Text style={Styles.textForm}>Télefono:</Text>
                        <TextInput style={Styles.inputForm} value={this.state.telefono}
                        onChangeText={(text) => this.setState({telefono:text})}></TextInput>
                    </View>
                    <View style={Styles.form}>
                        <Text style={Styles.textForm}>Cliente SAP:</Text>
                        <TextInput style={Styles.inputForm} value={this.state.sap}
                        onChangeText={(text) => this.setState({sap:text})}></TextInput>
                    </View>
                    <View style={Styles.form}>
                        <Text style={Styles.textForm}>Fecha creación:</Text>
                        <InputTime></InputTime>
                        {/* <TextInput style={Styles.inputForm} value={this.state.fechaCreacion}></TextInput> */}
                    </View>
                    <View style={Styles.form}>
                        <Text style={Styles.textForm}>Nombre fiscal:</Text>
                        <TextInput style={Styles.inputForm} value={this.state.nombre_fiscal}
                        onChangeText={(text) => this.setState({nombre_fiscal:text})}></TextInput>
                    </View>
                    <View style={Styles.form}>
                        <Text style={Styles.textForm}>RFC:</Text>
                        <TextInput style={Styles.inputForm} value={this.state.rfc}
                        onChangeText={(text) => this.setState({rfc:text})}></TextInput>
                    </View>
                    <View style={Styles.form}>
                        <Text style={Styles.textForm}>Contacto:</Text>
                        <TextInput style={Styles.inputForm} value={this.state.contacto}
                        onChangeText={(text) => this.setState({contacto:text})}></TextInput>
                    </View>
                    <View style={Styles.form}>
                        <Text style={Styles.textForm}>Estados:</Text>
                        
                        {this.state.estados.length > 0 && 
                            <Picker
                                selectedValue={this.state.idestado}
                                style={Styles.inputForm}
                                onValueChange={this.handleChange}
                            >
                                <Picker.Item key={0} label={"Selecciona un estado"} value={0} />
                                { this.state.estados.map((item) => (
                                    <Picker.Item key={item.idestado} label={item.nombre} value={item.idestado} />
                                ))}                            
                                
                            </Picker>
                        }
                    </View>
                    <View style={Styles.form}>
                        <Text style={Styles.textForm}>Municipio:</Text>
                        {this.state.municipios.length > 0 ? 
                            <Picker
                                selectedValue={this.state.idmunicipio}
                                style={Styles.inputForm}
                                onValueChange={this.handleChangeMunicipio}
                            > 
                                { this.state.municipios.map((item) => (
                                    <Picker.Item key={item.idmunicipio} label={item.nombre} value={item.idmunicipio} />
                                ))}                            
                                
                            </Picker> : <View style={Styles.inputForm}></View>
                        }
                    </View>
                    <View style={Styles.form}>
                        <Text style={Styles.textForm}>Latitud:</Text>
                        <TextInput style={Styles.inputForm} value={this.state.latitud} editable={false}></TextInput>
                    </View>
                    <View style={Styles.form}>
                        <Text style={Styles.textForm}>Longitud:</Text>
                        <TextInput style={Styles.inputForm} value={this.state.longitud} editable={false}></TextInput>
                    </View>
                    <View style={Styles.form}>
                        {this.state.isEdit ? <TouchableOpacity style={Styles.botonRegistro} onPress={this.updateCliente}><Text style={Styles.textButton}>Guardar</Text></TouchableOpacity> : <TouchableOpacity style={Styles.botonRegistro} onPress={this.addCliente}><Text style={Styles.textButton}>Registrar</Text></TouchableOpacity> }
                    </View>
                </View>
            </ScrollView>
        )
    }
}

export default NewCliente;