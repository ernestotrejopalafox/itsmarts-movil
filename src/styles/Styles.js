import {StyleSheet} from 'react-native'

const Styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'white'
    },
    body:{
        padding:10,
        margin:10, 
        flexDirection:'column',
        backgroundColor:'white',
        width:'95%',
        height:'95%'
    },
    title:
    {
        borderColor:'black',
        borderRadius:5,
        borderWidth:1,
        fontSize:25,
        fontWeight:'bold',
        alignContent:'center',
        textAlign:'center',
        color:'black'
    },
    flatList:
    {
        marginTop:10
    },
    headerForm:
    {
        flexDirection:'column',
        backgroundColor:'red',
        borderColor:'red',
        borderRadius:10,
        borderWidth:10,
        color:'red'
    },
    botonRegistro: 
    {
        marginTop:10,
        borderWidth:1,
        borderRadius:5,
        width:'60%',
        backgroundColor:'#28a745',
        height:40,      
        justifyContent:'center'  
    },
    textButton:
    {
        color:'white',
        fontSize:20,
        textAlign:'center',
        fontWeight:'bold'
    },
    form:
    {
        flexDirection:'row',
        width:'100%',
        justifyContent:'center',
        marginTop:10
    },
    textForm:
    {
        fontWeight:'bold',
        width:'40%',
        fontSize:20,
        marginTop:10,
        marginLeft:10

    },
    inputForm:
    {
        width:'55%',
        borderWidth:1,
        borderRadius:5,
        marginTop:5,
        marginBottom:5,
        marginEnd:5, 
        fontSize:20, 
        height:40,
        borderColor:'#C6C6C6'
    },
    section:
    {
       marginTop:20
    },
    item :
    {
        borderWidth:.1,
        padding:20,
        marginTop:10,
        shadowColor: "#F7F7F7",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
    },
    textInfo:
    {
        fontSize:18
    },
    botones:
    {
        flexDirection:"row",
        flex:1
    }
    ,
    colspanEdit:
    {
        width:'30%',
        backgroundColor:'#007bff',
        height:30,
        alignContent:'center',
        marginTop:10,
        borderRadius:5
    },
    colspanDelete:
    {
        width:'30%',
        backgroundColor:'#dc3545',
        height:30,
        alignContent:'center',
        marginTop:10,
        borderRadius:5
    }
});
  

export {Styles as default}