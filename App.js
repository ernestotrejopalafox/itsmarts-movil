import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Index from './src/views/Index';
import Cliente from './src/views/Cliente';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{
                        headerShown: false
                      }}  >
        <Stack.Screen name="Index" component={Index} />
        <Stack.Screen name="Cliente" component={Cliente} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;